var express = require('express');
var router = express.Router();
var validator = require("email-validator");

//API Methods
router.post('/contact', function (req, res) {
    var errors = {};
    if (!validator.validate(req.body.email)) {
        errors['email'] = 'Please enter valid email address';
    } 
    if (req.body.msg == null || req.body.msg.length <= 30) {
        errors['msg'] = 'Please enter message longer than 30 characters';
    } 
    if (Object.keys(errors).length > 0) {
        res.status(422).json({ message : 'There are some errors', errors: errors });
    } else {
        res.status(200).json({ message : 'Your message has been sent!', errors: errors });
    }
})

module.exports = router;
