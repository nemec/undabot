'use strict'
const webpack = require('webpack')
const { VueLoaderPlugin } = require('vue-loader')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
	mode: 'development',
	entry: [
		'./src/app.js'
	],
	devServer: {
		hot: true,
		watchOptions: {
			poll: true
		},
		port: 8080,
		historyApiFallback: true,
		proxy: {
		'/api': {
			target: 'http://localhost:3000'
		}
		}
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				use: 'vue-loader'
			},
			{
				test: /\.css$/,
				use: [
					'vue-style-loader',
					'css-loader'
				]
			},
			{
				test: /\.s(c|a)ss$/,
				use: [
					'vue-style-loader',
					'css-loader',
					{
						loader: 'sass-loader',
						// Requires sass-loader@^7.0.0
						options: {
							implementation: require('sass'),
							indentedSyntax: true // optional
						},
						// Requires >= sass-loader@^8.0.0
						options: {
							implementation: require('sass'),
							sassOptions: {
								indentedSyntax: true // optional
							},
						},
					},
				],
			},
			{
				test: /\.js$/,
				use: 'babel-loader'
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new VueLoaderPlugin(),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'index.html',
			inject: true
		})
	]
}